import React, { Component } from 'react';
import {
    View,
    Image,
    StyleSheet,
    Text,
    Button,
    TextInput,
    TouchableOpacity,
    Alert,
    ImageBackground,
    SafeAreaView,
    TextInputComponent
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import {
    scale,
    verticalScale,
    moderateScale
} from 'react-native-size-matters';

import axios from 'axios';

export default class loginpage extends React.Component {

    validateEmail = (email) => {
        var re = /^(([^<>()[]\.,;:s@"]+(.[^<>()[]\.,;:s@"]+)*)|(".+"))@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}])|(([a-zA-Z-0-9]+.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };

    validateName = alph => {
        var alpha = /^[A-Za-z]+$/;
        return alpha.test(alph);
    };
    validatePhone = numb => {
        var num = /^[0-9]{10}$/;
        return num.test(numb);
    };
    validateCode = code => {
        var numm = /^[0-9]{4}$/;
        return numm.test(code);
    };
    constructor() {
        super();
        this.state = {
            textInputName: '',
            textInputEmail: '',
            textInputCode: '',
            textInputPassword: '',
            textInputPhone: ''
        };
    }
    

//  componentDidMount() {
//         var payload = {
//            //data database ch send kr rhe

//             firstName: "seemma",
//             lastName: "kumari",
//             dob: "20-01-1999",
//             email: "semma@gmail.com",
//             password: "12349",
//             phone_Number: "1234567890",
//             country_Code: "1234"

//             //check krna login krke
//         //    email: "semma@gmail.com",
//         //    password: "12349"
//         }

//         axios.post('http://192.168.0.50:9988/login', payload)
//           .then(function (response) {
//         alert(JSON.stringify(response));
//           })
//           .catch(function (error) {
//             console.warn(error);
    //       });

        
    // }
    checkTextInput = () => {
      
        // const {
        //   textInputName,
        //   textInputEmail,
        //   textInputCode,
        //   textInputPhone,
        //   textInputPassword
        // } = this.state;
        // if (textInputName == '') {
        //   alert('please enter name')
        // }
        // else if (textInputEmail == '') {
        //   alert('please enter Email')
        // }
        // else if (!this.validateEmail(this.state.textInputEmail)) {
        //   alert('email must contain abc@gmail.com')
        // }
        // else if (textInputCode == '') {
        //   alert('please enter code')
        // }
        // else if (!this.validateCode(this.state.textInputCode)) {
        //   alert('must be in number')
        // }
        // else if (textInputPhone == '') {
        //   alert('please enter phone number')
        // }
        // // else if (!this.validateCode(this.state.textInputPhone)) {
        // //   alert('must be in number dfdg')
        // // }
        // else if (textInputPassword == '') {
        //   alert('please enter password')
        // }
        // else if (!this.validateCode(this.state.textInputPassword)) {
        //   alert('must be in number')
        // }else{
            var payload = {
                //data database ch send kr rhe
     
                 firstName: "seemma",
                 lastName: "kumari",
                 dob: "20-01-1999",
                 email: "semma@gmail.com",
                 password: "12349",
                 phone_Number: "1234567890",
                 country_Code: "1234"
     
                 //check krna login krke
             //    email: "semma@gmail.com",
             //    password: "12349"
             }
     
             axios.post('http://192.168.0.50:9988/login', payload)
               .then(function (response) {
             alert(JSON.stringify(response));
               })
               .catch(function (error) {
                 console.warn(error);
               });
     
             
        
    };
    render() {
        return (
            <SafeAreaView>
                <View
                    style={{
                        height: verticalScale(56),
                        width: '100%',
                        justifyContent: 'center',
                        alignContent: 'center',
                        backgroundColor: 'rgb(224,17,95)',

                    }}
                >
                    <Text style={{

                        fontSize: moderateScale(30),
                        textAlign: 'center',
                        marginHorizontal: moderateScale(16),
                        color: 'white'
                    }}>
                        SignUp
                    </Text>
                    <TouchableOpacity style={{
                        position: 'absolute'
                    }} onPress={() => this.props.navigation.goBack()} >

                    </TouchableOpacity>
                </View>
                <View>
                    <View style={{
                        marginTop: moderateScale(24)
                    }}>
                        <Text style={{
                            color: 'black',
                            fontSize: moderateScale(14),
                            marginStart: moderateScale(16),
                            marginTop: moderateScale(8)
                        }}>
                            Name
                </Text>
                        <TextInput style={{
                            height: verticalScale(40),
                            borderColor: 'black',
                            marginTop: moderateScale(4),
                            marginHorizontal: moderateScale(16),
                            fontWeight: "bold",
                            borderRadius: moderateScale(30),
                            borderBottomWidth: moderateScale(2)
                        }} placeholder='Enter Name'
                            onChangeText={textInputName => this.setState({
                                textInputName
                            })} />
                    </View>
                    <View style={{
                        marginTop: moderateScale(24)
                    }}>
                        <Text style={{
                            color: 'black',
                            fontSize: moderateScale(14),
                            marginStart: moderateScale(16),
                            marginTop: moderateScale(8)
                        }}>
                            Email
                </Text>
                        <TextInput style={{
                            height: verticalScale(40),
                            borderColor: 'black',
                            marginTop: moderateScale(8),
                            marginHorizontal: moderateScale(16),
                            fontWeight: "bold",
                            borderRadius: moderateScale(30),
                            borderBottomWidth: moderateScale(2)
                        }} placeholder='Enter Email'
                            onChangeText={textInputEmail => this.setState({
                                textInputEmail
                            })} />
                    </View>
                    <View style={{
                        marginTop: moderateScale(24)
                    }}>
                        <Text style={{
                            color: 'black',
                            fontSize: moderateScale(14),
                            marginStart: moderateScale(16),
                            marginTop: moderateScale(8)
                        }}>
                            country Code
                </Text>
                        <TextInput style={{
                            height: verticalScale(40),
                            borderColor: 'black',
                            marginTop: moderateScale(4),
                            marginHorizontal: moderateScale(16),
                            fontWeight: "bold",
                            borderRadius: moderateScale(30),
                            borderBottomWidth: moderateScale(2)
                        }} placeholder='Enter phone number'
                            onChangeText={textInputCode => this.setState({
                                textInputCode
                            })} />
                    </View>
                    <View style={{
                        marginTop: moderateScale(24)
                    }}>
                        <Text style={{
                            color: 'black',
                            fontSize: moderateScale(14),
                            marginStart: moderateScale(16),
                            marginTop: moderateScale(8)
                        }}>
                            Phone number
                </Text>
                        <TextInput style={{
                            height: verticalScale(40),
                            borderColor: 'black',
                            marginTop: moderateScale(4),
                            marginHorizontal: moderateScale(16),
                            fontWeight: "bold",
                            borderRadius: moderateScale(30),
                            borderBottomWidth: moderateScale(2)
                        }} placeholder='Enter phone number'
                            onChangeText={textInputPhone => this.setState({
                                textInputPhone
                            })} />
                    </View>
                    <View style={{
                        marginTop: moderateScale(24)
                    }}>
                        <Text style={{
                            color: 'black',
                            fontSize: moderateScale(14),
                            marginStart: moderateScale(16),
                            marginTop: moderateScale(20)
                        }}>
                            Password
                 </Text>
                        <TextInput style={{
                            height: verticalScale(40),
                            borderColor: 'black',
                            marginTop: moderateScale(8),
                            marginHorizontal: moderateScale(16),
                            fontWeight: "bold",
                            borderRadius: moderateScale(30),
                            borderBottomWidth: moderateScale(2)
                        }} placeholder="enter password" secureTextEntry={true} onChangeText={textInputPassword =>
                            this.setState({ textInputPassword })} />
                    </View>
                    <Text style={{
                        color: 'black',
                        fontSize: moderateScale(14),
                        marginStart: moderateScale(16),
                        marginTop: moderateScale(20)
                    }}>
                        Date Of birth
                 </Text>
                    <View style={styles.container}>
                        <DatePicker
                            style={{
                                width: 350,

                            }}
                            date={this.state.date} //initial date from state
                            mode="date" //The enum of date, datetime and time
                            placeholder="select date"
                            format="DD-MM-YYYY"
                            minDate="01-01-2016"
                            maxDate="01-01-2019"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 36
                                }
                            }}
                            onDateChange={(date) => { this.setState({ date: date }) }}
                        />
                    </View>

                    <View style={{
                        marginTop: moderateScale(24),
                        justifyContent: 'center'
                    }}>
                        <TouchableOpacity onPress={this.checkTextInput}
                            style={{
                                justifyContent: 'center',
                                paddingHorizontal: moderateScale(10),
                                backgroundColor: 'rgb(224,17,95)',
                                marginHorizontal: moderateScale(16),
                                alignContent: 'center',
                                height: verticalScale(48),
                                width: '50%',
                                padding: moderateScale(11),
                                marginTop: moderateScale(8),
                                marginLeft: moderateScale(100)
                            }}
                        >
                            <Text style={{
                                fontWeight: 'bold',
                                alignSelf: 'center',
                                fontSize: moderateScale(20)
                            }}>
                                SignUp
                  </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,


        marginTop: 8,
        padding: 16,
        marginHorizontal: 4,
    }
})